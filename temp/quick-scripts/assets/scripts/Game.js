(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/scripts/Game.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '331daKfep5BdpeJPskRj/YW', 'Game', __filename);
// scripts/Game.js

"use strict";

cc.Class({
  extends: cc.Component,

  properties: {
    // this property quotes the PreFab resource of stars
    starPrefab: {
      default: null,
      type: cc.Prefab
    },
    // the random scale of disappearing time for stars
    maxStarDuration: 0,
    minStarDuration: 0,
    // ground node for confirming the height of the generated star's position
    ground: {
      default: null,
      type: cc.Node
    },
    // player node for obtaining the jump height of the main character and controlling the movement switch of the main character
    player: {
      default: null,
      type: cc.Node
    },
    // reference of score label
    scoreDisplay: {
      default: null,
      type: cc.Label
    }
  },

  // LIFE-CYCLE CALLBACKS:

  onLoad: function onLoad() {
    this.groundY = this.ground.y + this.ground.height / 2;

    // initialize timer
    this.timer = 0;
    this.starDuration = 0;

    // generate a new star
    this.spawnNewStar();

    // initialize scoring
    this.score = 0;

    var IsLoad = true;
    this.IsLoad = IsLoad;
  },

  spawnNewStar: function spawnNewStar() {
    // generate a new node in the scene with a preset template
    var newStar = cc.instantiate(this.starPrefab);
    // put the newly added node under the Canvas node
    this.node.addChild(newStar);
    // set up a random position for the star
    newStar.setPosition(this.getNewStarPosition());
    // Staging a reference of Game object on a star component
    newStar.getComponent("Star").game = this;

    // reset timer, randomly choose a value according the scale of star duration
    this.starDuration = this.minStarDuration + Math.random() * (this.maxStarDuration - this.minStarDuration);
    this.timer = 0;
  },

  getNewStarPosition: function getNewStarPosition() {
    var randX = 0;
    // According to the position of the ground level and the main character's jump height, randomly obtain an anchor point of the star on the y axis
    var randY = this.groundY + Math.random() * this.player.getComponent("Player").jumpHeight + 50;
    // according to the width of the screen, randomly obtain an anchor point of star on the x axis
    var maxX = this.node.width / 2;
    randX = (Math.random() - 0.5) * 2 * maxX;
    // return to the anchor point of the star
    return cc.v2(randX, randY);
  },

  gainScore: function gainScore() {
    this.score += 1;
    // update the words of the scoreDisplay Label
    this.scoreDisplay.string = "Score: " + this.score;
  },

  gameOver: function gameOver() {
    this.player.stopAllActions(); //stop the jumping action of the player node
    cc.director.loadScene("game");
  },

  update: function update(dt) {
    // update timer for each frame, when a new star is not generated after exceeding duration
    // invoke the logic of game failure
    cc.log("Game.js::update()");
    if (this.timer > this.starDuration && this.IsLoad === true) {
      //this.gameOver();
      cc.log("gameOver");
      this.IsLoad = false;
      return;
    }
    this.timer += dt;
  }
});

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=Game.js.map
        