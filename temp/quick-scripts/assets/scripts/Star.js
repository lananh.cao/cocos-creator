(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/scripts/Star.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '2ec1dfazBhNU5Lrgj7hSa26', 'Star', __filename);
// scripts/Star.js

"use strict";

cc.Class({
  extends: cc.Component,

  properties: {
    // When the distance between the star and main character is less than this value, collection of the point will be completed
    pickRadius: 0
  },

  onLoad: function onLoad() {
    // mong muốn hiện tại của log sẽ là
    // game
    // spawnNewStar
    // star
    console.log("Star.js::onLoad()");
  },

  checkGame: function checkGame() {
    console.log("checkGame", this.game);
  },

  getPlayerDistance: function getPlayerDistance() {
    // judge the distance according to the position of the player node
    // vì onLoad của game.js được gọi trước => spawnNewStar được gọi
    // => this.game lúc này đã có giá trị
    var playerPos = this.game.player.getPosition();
    // calculate the distance between two nodes according to their positions
    var dist = this.node.position.sub(playerPos).mag();
    return dist;
  },

  onPicked: function onPicked() {
    // When the stars are being collected, invoke the interface in the Game script to generate a new star
    this.game.spawnNewStar();
    // invoke the scoring method of the Game script
    this.game.gainScore();
    // then destroy the current star's node
    this.node.destroy();
  },

  update: function update(dt) {
    // judge if the distance between the star and main character is less than the collecting distance for each frame
    if (this.getPlayerDistance() < this.pickRadius) {
      // invoke collecting behavior
      this.onPicked();
      return;
    }
  }
});

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=Star.js.map
        