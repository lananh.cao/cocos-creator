cc.Class({
  extends: cc.Component,

  properties: {
    // When the distance between the star and main character is less than this value, collection of the point will be completed
    pickRadius: 0
  },

  onLoad: function() {
    // mong muốn hiện tại của log sẽ là
    // game
    // spawnNewStar
    // star
    console.log("Star.js::onLoad()");
  },

  checkGame: function() {
    console.log("checkGame", this.game);
  },

  getPlayerDistance: function() {
    // judge the distance according to the position of the player node
    // vì onLoad của game.js được gọi trước => spawnNewStar được gọi
    // => this.game lúc này đã có giá trị
    var playerPos = this.game.player.getPosition();
    // calculate the distance between two nodes according to their positions
    var dist = this.node.position.sub(playerPos).mag();
    return dist;
  },

  onPicked: function() {
    // When the stars are being collected, invoke the interface in the Game script to generate a new star
    this.game.spawnNewStar();
    // invoke the scoring method of the Game script
    this.game.gainScore();
    // then destroy the current star's node
    this.node.destroy();
  },

  update: function(dt) {
    // judge if the distance between the star and main character is less than the collecting distance for each frame
    if (this.getPlayerDistance() < this.pickRadius) {
      // invoke collecting behavior
      this.onPicked();
      return;
    }
  }
});
